# Lab : Création et gestion d'un pod statique dans Kubernetes



# Objectifs

1. Créer un manifeste pour un pod statique

2. Démarrez le module statique

# Contexte

ous travaillez pour BeeBox, une entreprise qui assure des expéditions régulières d'abeilles aux clients.

La société a construit un outil de diagnostic spécial pour ses nœuds K8s. Cet outil peut être exécuté en tant que conteneur dans un pod K8s et collecte des données de diagnostic détaillées à partir du nœud de travail tout au long du cycle de vie du nœud.

Une fonctionnalité particulièrement utile est que cet outil est capable de collecter des données pendant le processus de démarrage du nœud, avant que le kubelet ne commence à communiquer avec l'API Kubernetes, ou même avant qu'un kubelet ne rejoigne un cluster. Pour en bénéficier, le Pod doit fonctionner sans dépendre de la présence d'une connexion au serveur API Kubernetes. Il devra être exécuté et géré directement par le kubelet.

Votre tâche consiste à créer un pod pour exécuter cet outil de diagnostic sur le `Worker Node 1serveur`. Utilisez l'image `acgorg/beebox-diagnostic:1` pour ce Pod.

>![Alt text](img/image.png)

# Application

Suivez les étapes ci-dessous pour créer et gérer un pod statique dans votre cluster Kubernetes.

## 1. Connexion au serveur Worker Node 1

Connectez-vous au serveur Worker Node 1 à l'aide des informations d'identification fournies :

```sh
ssh -i id_rsa user@PUBLIC_IP_ADDRESS_WORKER1
```

## 2. Création d'un fichier manifeste de pod statique

1. Créez un fichier manifeste YAML pour le pod statique :

```sh
sudo nano /etc/kubernetes/manifests/beebox-diagnostic.yml
```

Ajoutez le contenu suivant au fichier :

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: beebox-diagnostic
spec:
  containers:
  - name: beebox-diagnostic
    image: acgorg/beebox-diagnostic:1
    ports:
    - containerPort: 80
```

2. Enregistrez et quittez le fichier

## 3. Démarrage du pod statique

Redémarrez kubelet pour démarrer le pod statique :

```sh
sudo systemctl restart kubelet
```

>![Alt text](img/image-1.png)
*Reboot du service réussi*

## 4. Vérification du pod statique

Dans une nouvelle session de terminal, connectez-vous au serveur Control Plane Node :

```sh
ssh -i id_rsa user@PUBLIC_IP_ADDRESS_CONTROL_NODE
```

Vérifiez l'état de votre pod statique :

```sh
kubectl get pods
```

>![Alt text](img/image-2.png)
*Pod static en cours d'exécution*

Vous devriez voir le pod `beebox-diagnostic...`.

## 5. Suppression du pod statique via l'API k8s

Essayez de supprimer le pod statique à l'aide de l'API Kubernetes :

```sh
kubectl delete pod beebox-diagnostic-k8s-worker1
```

>![Alt text](img/image-3.png)
*Suppression du pod static*

## 6. Vérification de l'état du pod

Vérifiez l'état du pod après la tentative de suppression :

```sh
kubectl get pods
```

>![Alt text](img/image-4.png)
*Le pod à été recréé*

Vous verrez que le pod a été immédiatement recréé, car il s'agit d'un pod miroir créé par le kubelet pour représenter le pod statique, et donc il est impossible de le supprimer.

# Résumé

Dans ce lab, vous avez appris à :

1. Vous connecter à un serveur Worker Node.
2. Créer un fichier manifeste pour un pod statique.
3. Démarrer un pod statique en redémarrant kubelet.
4. Vérifier l'état du pod statique via l'API Kubernetes.
5. Observer le comportement de recréation automatique des pods miroirs.

Félicitations, vous avez terminé ce lab !